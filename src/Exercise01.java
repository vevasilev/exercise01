public class Exercise01 {
    public static void main(String[] args) {
        int studentIDCard = 0x1FC37;
        long numberPhone = 89999999999L;
        int twoLastDigitsNumberPhone = 0b1100011;
        int fourLastDigitsNumberPhone = 023417;
        int numberStudent = 3;
        int modifiedNumberStudent = (numberStudent - 1) % 26 + 1;
        char symbol = (char) (64 + modifiedNumberStudent);

        System.out.println("studentIDCard: " + studentIDCard);
        System.out.println("numberPhone: " + numberPhone);
        System.out.println("twoLastDigitsNumberPhone: " + twoLastDigitsNumberPhone);
        System.out.println("fourLastDigitsNumberPhone: " + fourLastDigitsNumberPhone);
        System.out.println("modifiedNumberStudent: " + modifiedNumberStudent);
        System.out.println("symbol: " + symbol);
    }
}
